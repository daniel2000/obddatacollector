package com.example.bluetooth2;

import android.content.Context;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

//https://mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
public class CSV {

    private float[][] prevlinearAccData1D = new float[1][150];
    private float[][][] prevlinearAccData3D = new float[1][50][3];
    private boolean firstIteration = true;

    public void writeToCSV(Context context, String fileName, String commaSeperatedData) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //Checking the availability state of the External Storage.
                String state = Environment.getExternalStorageState();
                if (!Environment.MEDIA_MOUNTED.equals(state)) {

                    //If it isn't mounted - we can't write into it.
                    return;
                }

                //Create a new file that points to the root directory, with the given name:
                File file = new File(context.getExternalFilesDir(null), fileName);

                //This point and below is responsible for the write operation
                FileOutputStream outputStream = null;
                try {
                    file.createNewFile();
                    //second argument of FileOutputStream constructor indicates whether
                    //to append or create new file if one exists
                    outputStream = new FileOutputStream(file, true);

                    outputStream.write(commaSeperatedData.getBytes());
                    outputStream.flush();
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    public void writeArrayToCSV(Context context, String fileName, float[][][] array3D, int predicatedEvent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //Checking the availability state of the External Storage.
                String state = Environment.getExternalStorageState();
                if (!Environment.MEDIA_MOUNTED.equals(state)) {

                    //If it isn't mounted - we can't write into it.
                    return;
                }

                //Create a new file that points to the root directory, with the given name:
                File file = new File(context.getExternalFilesDir(null), fileName);

                //This point and below is responsible for the write operation
                FileOutputStream outputStream = null;
                try {
                    file.createNewFile();
                    //second argument of FileOutputStream constructor indicates whether
                    //to append or create new file if one exists
                    outputStream = new FileOutputStream(file, true);

                    outputStream.write(("Event Detected:" + String.valueOf(predicatedEvent) + "\n").getBytes());
                    StringBuilder temp = new StringBuilder();
                    for(int i = 0;i< array3D.length;i++) {
                        for (int j = 0;j<array3D[i].length;j++) {
                            temp = new StringBuilder();
                            for (int k = 0;k<array3D[i][j].length;k++){
                                temp.append(array3D[i][j][k]).append(",");

                            }
                            temp.setLength(temp.length()-1);
                            temp.append("\n");
                            outputStream.write(temp.toString().getBytes());
                        }
                    }

                    outputStream.write("\n\n".getBytes());

                    outputStream.flush();
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public TupleArrayInt readCSV(String inputFilePath, Context context, int lastReadRow) {
        //Create a new file that points to the root directory, with the given name:
        File file = new File(context.getExternalFilesDir(null), inputFilePath);

        String line = "";
        int counter = 0;
        int innerCounter =0;
        int readLimit = 49;

        TupleArrayInt output = new TupleArrayInt();
        output.array3D = new float[1][50][3];



        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null && readLimit > -1) {
                if (counter > lastReadRow) {
                    // use comma as separator
                    String[] dataPoint = line.split(",");

                    output.array3D[0][readLimit][0] = Float.parseFloat(dataPoint[7]);
                    output.array3D[0][readLimit][1] = Float.parseFloat(dataPoint[8]);
                    output.array3D[0][readLimit][2] = Float.parseFloat(dataPoint[9]);

                    lastReadRow++;
                    readLimit--;
                }

                counter++;

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        output.lastRow = lastReadRow;

        return output;
    }


}

/*
 String[] inputArray = new String[50];

        try {
            File inputFile = new File(inputFilePath);
            InputStream inputStream = new FileInputStream(inputFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));


            Stream out = bufferedReader.lines().skip(1);
            bufferedReader.close();

        } catch (IOException e ) {
            e.printStackTrace();
        }

 */