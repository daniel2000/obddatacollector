package com.example.bluetooth2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.SensorEventListener;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TrainingProcedure extends AppCompatActivity {

    TextView centerText;
    private Boolean storeData = false;
    private int arrayCounter;
    private float[][] prevlinearAccData1D = new float[1][150];
    private float[][][] prevlinearAccData3D = new float[1][50][3];
    boolean model3D = false;
    String[] eventPrompts = new String[7];
    int currentEventNo = 0;
    Button trainingBtn;
    SensorEventListener accEventListener;
    Thread predictionsThread;
    Thread sensorsThread;
    SensorsRunnable sensorsRunnable;
    boolean predictionsThreadRun = true;
    CSV csvHandler = new CSV();
    int modelType = 0;

    int lastRowRead = 0;

    float prevX = 0;
    float prevY = 0;
    float prevZ = 0;

    TextView listLengthText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Driving Behaviour Monitor");
        setContentView(R.layout.activity_training_procedure);
        centerText = (TextView) findViewById(R.id.centerText);

        arrayCounter = 49;
        /*
        if (model3D) {
            arrayCounter = 49;
        } else {
            arrayCounter = 147;
        }

         */


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        trainingBtn = (Button) findViewById(R.id.continueBtn);
        listLengthText = (TextView) findViewById(R.id.listCount);


        eventPrompts[0] = "Drive normally";
        eventPrompts[1] = "Hard Right";
        eventPrompts[2] = "Hard Left";
        eventPrompts[3] = "Accelerate";
        eventPrompts[4] = "Brake";
        eventPrompts[5] = "Change to Left Lane";
        eventPrompts[6] = "Change to Right Lane";


        sensorsRunnable = new SensorsRunnable(getApplicationContext());
        sensorsThread = new Thread(sensorsRunnable);

        sensorsThread.start();

        TrainingPredictionsRunnable predictionsRunnable = new TrainingPredictionsRunnable(sensorsRunnable);
        predictionsThread = new Thread(predictionsRunnable);
        predictionsThread.start();

    }

    public void startTraining(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {

                changeButtonStatus(false);

                startCountAnimation(eventPrompts[currentEventNo]);

                try {
                    // 4000 milliseconds to wait for the countdown timer
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                storeData = true;

                String temp = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + currentEventNo + "\n";
                csvHandler.writeToCSV(getApplicationContext(),"Events/Events" + LocalDate.now()+ ".csv",temp);

                try {
                    // 5000 milliseconds for the event to occur
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                temp = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + ",-" + currentEventNo + "\n";
                csvHandler.writeToCSV(getApplicationContext(),"Events/Events" + LocalDate.now() +".csv",temp);

                storeData = false;

                changeButtonStatus(true);

                if (currentEventNo == 0) {
                    setButtonText("Continue");
                }

                if(currentEventNo == 6) {
                    currentEventNo = 0;
                    predictionsThreadRun = false;

                    displayMessage("Data Collection Halted");

                }
                currentEventNo++;

            }
        }).start();
    }

    private void displayMessage(String message) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                centerText.setText(message);
            }
        });
    }

    private void setButtonText(String message) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                trainingBtn.setText(message);
            }
        });
    }



    private void changeButtonStatus(Boolean enabled) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                if(enabled){
                    trainingBtn.setBackgroundColor(Color.parseColor("#008000"));
                    trainingBtn.setEnabled(enabled);
                } else {
                    trainingBtn.setBackgroundColor(Color.parseColor("#FF0000"));
                    trainingBtn.setEnabled(enabled);
                }
            }
        });
    }

    private void changeArrayCounter(int listLength){
        this.runOnUiThread(new Runnable() {
            public void run() {
                listLengthText.setText(String.valueOf(listLength));
            }
        });
    }

    private void startCountAnimation(String eventPrompt) {
        this.runOnUiThread(new Runnable() {
            public void run() {

                new CountDownTimer(4000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        centerText.setText("In " + millisUntilFinished / 1000 + " seconds\n"+eventPrompt);
                    }

                    public void onFinish() {
                        centerText.setText(eventPrompt.toUpperCase() + "!");
                    }
                }.start();

            }
        });


    }


    public static  TupleStringInt mostCommon(List<Integer> list) {
        //https://stackoverflow.com/questions/19031213/java-get-most-common-element-in-a-list
        Map<Integer, Integer> map = new HashMap<>();

        for (Integer t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<Integer, Integer> max = null;


        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue()) {
                max = e;
            }
        }
        
        Float probability = 0.0f;
        probability = (float) max.getValue()/list.size();

        String probabilityColour = "";


        if (probability <= 0.5) {
            // Yellow
            probabilityColour = "#DED947";
        } else if (0.5 <= probability && probability <= 0.75) {
            // Orange
            probabilityColour = "#FFA500";
        } else if (probability > 0.75) {
            // Red
            probabilityColour = "#FF0000";
        } else {
            // Black
            probabilityColour = "#000000";
        }



        TupleStringInt returnObj = new TupleStringInt();
        returnObj.colour = probabilityColour;
        returnObj.predicatedEvent =  max.getKey();

        return returnObj;
    }



    public void changeModel(View view) {
        RadioButton cnnModel = (RadioButton) findViewById(R.id.radioCNNmodel);
        RadioButton exactModel = (RadioButton) findViewById(R.id.radioExactModel);
        if (cnnModel.isChecked()) {
            modelType = 0;
        }
        else if(exactModel.isChecked()){
            modelType = 1;
        }
    }

    public class TrainingPredictionsRunnable implements Runnable {
        private SensorsRunnable sensorSet;
        public TrainingPredictionsRunnable(SensorsRunnable sensorSet) {
            this.sensorSet = sensorSet;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void run() {

            TensorFlowLiteMethods tensorFlowLiteMethods = new TensorFlowLiteMethods();
            String output;

            TupleStringInt returnObj = new TupleStringInt();

            while(predictionsThreadRun){

                returnObj.predicatedEvent = -1;

                /*
                if (sensorSet.prevlinearAccData3DList.size() != 0) {
                    csvHandler.writeArrayToCSV(getApplicationContext(),"ArraysFedToModel" + LocalDate.now() + ".csv",sensorSet.prevlinearAccData3DList.get(0),returnObj.predicatedEvent);
                }

                 */


                /*
                if (storeData) {
                    if (sensorSet.prevlinearAccData3DList.size() != 0) {
                        // Date, Time, NanoTime, X, Y, Z, Predicted Event, True Event
                        output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + sensorSet.prevlinearAccData3DList.get(0)[0][0][0] + "," + sensorSet.prevlinearAccData3DList.get(0)[0][0][1] + "," + sensorSet.prevlinearAccData3DList.get(0)[0][0][2] + "," + returnObj.predicatedEvent + "," + currentEventNo + "\n";

                        csvHandler.writeToCSV(getApplicationContext(),"EventsWithAccData1" + LocalDate.now() + ".csv", output);
                    }

                }

                 */
                //changeArrayCounter(sensorSet.prevlinearAccData3DList.size());


               // if (sensorSet.prevlinearAccData3DList.size() > 1) {
                    //List<Integer> results = new ArrayList<>();

                    /*
                    //https://howtodoinjava.com/java/collections/arraylist/arraylist-clone-deep-copy/
                    Iterator<float[][][]> iterator = sensorSet.prevlinearAccData3DList.iterator();
                    while(iterator.hasNext()){
                        
                        //tempCopyOfAccDataList.add((float[][][]) iterator.next().clone());
                        tempCopyOfAccDataList.add(new float[][][]{iterator.next()[0]});
                    }

                    */
                    //ArrayList<float[][][]> tempCopyOfAccDataList = new ArrayList<>();
                    //tempCopyOfAccDataList = deepCopyArrayList(sensorSet.prevlinearAccData3DList);
                    //sensorSet.prevlinearAccData3DList.clear();
                    //
                    /*
                    tempCopyOfAccDataList.get(0)[0][0][0] = 0.123456789f;
                    sensorSet.prevlinearAccData3DList.get(0)[0][0][0] = 0.987654321f;



                    for (int i = 0;i<tempCopyOfAccDataList.size();i++) {
                        if(modelType == 0) {
                            //CNN
                            returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(),null, tempCopyOfAccDataList.get(i),"CNN-Acc0.9683845639228821",true);
                        }

                        else if (modelType == 1) {
                            // Exact like paper
                            returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(),null, tempCopyOfAccDataList.get(i),"ExactCopyofModel4-10GRUNeurons7OutputUnits32BatchsizeSoftmaxactivation--Acc0.999133825302124",true);
                        }




                        if (prevX == tempCopyOfAccDataList.get(i)[0][0][0]) {
                            System.out.println(prevX);
                            System.out.println(tempCopyOfAccDataList.get(i)[0][0][0]);
                            System.out.println("HereX");
                        }
                        if(prevY == tempCopyOfAccDataList.get(i)[0][0][1]) {
                            System.out.println(prevY);
                            System.out.println(tempCopyOfAccDataList.get(i)[0][0][1]);
                            System.out.println("HereY");

                        }
                        if (prevZ == tempCopyOfAccDataList.get(i)[0][0][2]) {
                            System.out.println(prevZ);
                            System.out.println(tempCopyOfAccDataList.get(i)[0][0][2]);
                            System.out.println("HereZ");
                        }



                        prevX = tempCopyOfAccDataList.get(i)[0][0][0];
                        prevY = tempCopyOfAccDataList.get(i)[0][0][1];
                        prevZ = tempCopyOfAccDataList.get(i)[0][0][2];



                        output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + tempCopyOfAccDataList.get(i)[0][0][0] + "," + tempCopyOfAccDataList.get(i)[0][0][1] + "," + tempCopyOfAccDataList.get(i)[0][0][2] + "," + returnObj.predicatedEvent + "\n";


                        csvHandler.writeToCSV(getApplicationContext(),"AllPredications" + LocalDate.now() + ".csv",output);

                        results.add(returnObj.predicatedEvent);
                    }


                    returnObj = mostCommon(results);

                }
                */
                /*
                if (storeData) {
                    if (sensorSet.prevlinearAccData3DList.size() != 0) {
                        // Date, Time, NanoTime, X, Y, Z, Predicted Event, True Event
                        output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + sensorSet.prevlinearAccData3DList.get(0)[0][0][0] + "," + sensorSet.prevlinearAccData3DList.get(0)[0][0][1] + "," + sensorSet.prevlinearAccData3DList.get(0)[0][0][2] + "," + returnObj.predicatedEvent + "," + currentEventNo + "\n";

                        csvHandler.writeToCSV(getApplicationContext(),"EventsWithAccData1" + LocalDate.now() + ".csv", output);
                    }

                }

                 */

                TupleArrayInt arrayInput = new TupleArrayInt();
                List<Integer> results = new ArrayList<>();



                for (int i = 0;i<32;i++) {
                    arrayInput = csvHandler.readCSV("Accelerometer/ACCELEROMETER" + LocalDate.now() + ".csv",getApplicationContext(),lastRowRead);

                    if (lastRowRead + 50 == arrayInput.lastRow) {
                        lastRowRead = arrayInput.lastRow-49;
                        if(modelType == 0) {
                            //CNN
                            returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(),null, arrayInput.array3D,"ExactCopy4-Acc0.9896265268325806",true);
                        }

                        else if (modelType == 1) {
                            // Exact like paper
                            returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(),null, arrayInput.array3D,"ExactCopyofModel4-10GRUNeurons7OutputUnits32BatchsizeSoftmaxactivation--Acc0.999133825302124",true);
                        }

                        output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + arrayInput.array3D[0][0][0] + "," + arrayInput.array3D[0][0][1] + "," + arrayInput.array3D[0][0][2] + "," + returnObj.predicatedEvent + "\n";


                        csvHandler.writeToCSV(getApplicationContext(),"AllPredictions/AllPredications" + LocalDate.now() + ".csv",output);

                        results.add(returnObj.predicatedEvent);
                    } else {
                        //Array is not filled up and hence neither will the rest going forward
                        break;
                    }

                }


                if(results.size() > 0){
                    returnObj = mostCommon(results);
                }


                if (returnObj.predicatedEvent != -1 && returnObj.colour != null) {
                    RadioButton nonaggressive = (RadioButton) findViewById(R.id.nonAggressiveBtn1);
                    RadioButton rightTurn = (RadioButton) findViewById(R.id.aggRightTurnBtn1);
                    RadioButton leftTurn = (RadioButton) findViewById(R.id.aggLeftTurnBtn1);
                    RadioButton accelerating = (RadioButton) findViewById(R.id.aggAccelerationBtn1);
                    RadioButton braking = (RadioButton) findViewById(R.id.aggBrakingBtn1);
                    RadioButton leftLaneChange = (RadioButton) findViewById(R.id.aggLeftLandChangeBtn1);
                    RadioButton rightLaneChange = (RadioButton) findViewById(R.id.aggRightLandChangeBtn1);


                    if (returnObj.predicatedEvent == 0) {
                        nonaggressive.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 1) {
                        rightTurn.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 2) {
                        leftTurn.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 3) {
                        accelerating.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 4) {
                        braking.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 5) {
                        leftLaneChange.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 6) {
                        rightLaneChange.setBackgroundColor(Color.parseColor(returnObj.colour));
                    }


                    // Delay
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }


                    nonaggressive.setBackgroundColor(Color.rgb(0, 255, 0));
                    rightTurn.setBackgroundColor(Color.rgb(0, 255, 0));
                    leftTurn.setBackgroundColor(Color.rgb(0, 255, 0));
                    accelerating.setBackgroundColor(Color.rgb(0, 255, 0));
                    braking.setBackgroundColor(Color.rgb(0, 255, 0));
                    leftLaneChange.setBackgroundColor(Color.rgb(0, 255, 0));
                    rightLaneChange.setBackgroundColor(Color.rgb(0, 255, 0));
                }
            }
        }
    }

    private ArrayList<float[][][]> deepCopyArrayList(ArrayList<float[][][]> arrayList){
        if(arrayList == null) {
            return null;
        }
        
        int sizeofArrayList = arrayList.size()-1;
        ArrayList<float[][][]> tempCopyOfAccDataList = new ArrayList<>();

        float[][][] temp = new float[1][50][3];
        for (int i = 0;i<sizeofArrayList;i++) {
            for(int j = 0;j<50;j++) {
                temp[0][j][0] = arrayList.get(0)[0][j][0];
                temp[0][j][1] = arrayList.get(0)[0][j][1];
                temp[0][j][2] = arrayList.get(0)[0][j][2];
            }
            tempCopyOfAccDataList.add(temp);
            arrayList.remove(0);



        }

        return tempCopyOfAccDataList;
    }

    public void changeDisplay(TupleStringInt returnObj) {
        this.runOnUiThread(new Runnable() {
            public void run() {

                RadioButton nonaggressive = (RadioButton) findViewById(R.id.nonAggressiveBtn1);
                RadioButton rightTurn = (RadioButton) findViewById(R.id.aggRightTurnBtn1);
                RadioButton leftTurn = (RadioButton) findViewById(R.id.aggLeftTurnBtn1);
                RadioButton accelerating = (RadioButton) findViewById(R.id.aggAccelerationBtn1);
                RadioButton braking = (RadioButton) findViewById(R.id.aggBrakingBtn1);
                RadioButton leftLaneChange = (RadioButton) findViewById(R.id.aggLeftLandChangeBtn1);
                RadioButton rightLaneChange = (RadioButton) findViewById(R.id.aggRightLandChangeBtn1);


                if (returnObj.predicatedEvent == 0) {
                    nonaggressive.setBackgroundColor(Color.parseColor(returnObj.colour));
                } else if (returnObj.predicatedEvent == 1) {
                    rightTurn.setBackgroundColor(Color.parseColor(returnObj.colour));
                } else if (returnObj.predicatedEvent == 2) {
                    leftTurn.setBackgroundColor(Color.parseColor(returnObj.colour));
                } else if (returnObj.predicatedEvent == 3) {
                    accelerating.setBackgroundColor(Color.parseColor(returnObj.colour));
                } else if (returnObj.predicatedEvent == 4) {
                    braking.setBackgroundColor(Color.parseColor(returnObj.colour));
                } else if (returnObj.predicatedEvent == 5) {
                    leftLaneChange.setBackgroundColor(Color.parseColor(returnObj.colour));
                } else if (returnObj.predicatedEvent == 6) {
                    rightLaneChange.setBackgroundColor(Color.parseColor(returnObj.colour));
                }


                // Delay
                try
                {
                    Thread.sleep(2000);
                }
                catch(InterruptedException ex)
                {
                    Thread.currentThread().interrupt();
                }



                nonaggressive.setBackgroundColor(Color.rgb(0,255,0));
                rightTurn.setBackgroundColor(Color.rgb(0,255,0));
                leftTurn.setBackgroundColor(Color.rgb(0,255,0));
                accelerating.setBackgroundColor(Color.rgb(0,255,0));
                braking.setBackgroundColor(Color.rgb(0,255,0));
                leftLaneChange.setBackgroundColor(Color.rgb(0,255,0));
                rightLaneChange.setBackgroundColor(Color.rgb(0,255,0));
            }
        });

    }



    /*
    private void sensors() {
        new Thread(new Runnable() {
            SensorManager sensorManager = (SensorManager) getApplicationContext().getSystemService(Context.SENSOR_SERVICE);
            private Sensor accSensor;
            int counter = 0;
            @Override
            public void run() {

                accEventListener = new SensorEventListener() {
                    TupleStringInt returnObj = new TupleStringInt();

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onSensorChanged(SensorEvent event) {
                        String output = "";
                        returnObj.predicatedEvent = -1;
                        if (arrayCounter != 0) {
                            prevlinearAccData[0][arrayCounter] = event.values[0];
                            prevlinearAccData[0][arrayCounter+1] = event.values[1];
                            prevlinearAccData[0][arrayCounter+2] = event.values[2];
                            arrayCounter = arrayCounter - 3;
                        } else if (arrayCounter == 0) {

                            arrayCounter = 150;

                            TensorFlowLiteMethods tensorFlowLiteMethods = new TensorFlowLiteMethods();

                            returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(),prevlinearAccData);

                            // Clearing the array
                            for( int i = 0; i < prevlinearAccData.length; i++ )
                                Arrays.fill( prevlinearAccData[i], 0 );

                        }


                        if (storeData) {
                            // Date, Time, NanoTime, X, Y, Z, Predicted Event, True Event
                            output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "," + returnObj.predicatedEvent + "," + currentEventNo + "\n";

                            WritingToCSV csvFile = new WritingToCSV(getApplicationContext(),"EventsWithAccData.csv", output);

                        }

                        if(returnObj.colour != null && returnObj.predicatedEvent !=-1) {
                            changeDisplay(returnObj);
                        }



                    }

                    @Override
                    public void onAccuracyChanged(Sensor sensor, int accuracy) {
                        Log.d("MY_APP", sensor.toString() + " - " + accuracy);
                    }
                };

                accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                // 50Hz sampling rate. ie 20000 microseconds
                sensorManager.registerListener(accEventListener, accSensor, 20000);

            }


        }).start();
    }

     */
}
