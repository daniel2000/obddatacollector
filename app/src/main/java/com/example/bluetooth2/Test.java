package com.example.bluetooth2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.TextView;

public class Test extends AppCompatActivity {

    TextView centerText;
    private Boolean storeData = false;
    private int currentEvent;
    private int arrayCounter = 150;
    private float[][] prevlinearAccData = new float[1][153];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        centerText = (TextView) findViewById(R.id.centerText);
        arrayCounter = 150;
    }

    public void startTraining(View view) throws InterruptedException {

        String[] eventPrompts = new String[7];
        eventPrompts[0] = "Accelerate";
        eventPrompts[1] = "Brake";
        eventPrompts[2] = "Hard Right";
        eventPrompts[3] = "Hard Left";
        eventPrompts[4] = "Change to Right Lane";
        eventPrompts[5] = "Change to Left Lane";
        eventPrompts[6] = "Drive normally";

        for(int i = 0;i<7;i++) {
            final int temp = i;
            centerText.setText(eventPrompts[temp]);
            this.runOnUiThread(new Runnable() {
                public void run() {

                    centerText.setText(eventPrompts[temp]);
                }
            });

            SystemClock.sleep(1000);


        }


    }
}