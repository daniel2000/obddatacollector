package com.example.bluetooth2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import org.tensorflow.lite.Interpreter;

import java.io.File;

public class TensorFlow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tensor_flow);

        File model = new File("C:\\Users\\attar\\AndroidStudioProjects\\Bluetooth2\\app\\src\\main\\res\\model.tflite");
        String output= "";

        try (Interpreter interpreter = new Interpreter(model)) {
            interpreter.run(1, output);
        }
    }
}