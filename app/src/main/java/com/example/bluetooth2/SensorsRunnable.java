package com.example.bluetooth2;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class SensorsRunnable implements Runnable {
    private SensorManager sensorManager;
    private Sensor linearAccSensor;
    private Sensor accSensor;
    private Sensor gyroscopeSensor;
    private Sensor magneticSensor;
    private Sensor gravitySensor;
    private SensorEventListener linearAccEventListener;
    private SensorEventListener accEventListener;
    private SensorEventListener gyroscopeEventListener;
    private SensorEventListener magneticFieldEventListener;
    private SensorEventListener gravityEventListener;

    private float[] currentMagneticField = new float[3];
    private float[] currentGravity = new float[16];

    private Context context;
    CSV csvHandler = new CSV();
    public ArrayList<float[][][]> prevAccData3DList = new ArrayList<>();
    public ArrayList<float[][][]> prevlinearAccData3DList = new ArrayList<>();
    int acc3DArrayPosition = 0;
    int linearAcc3DArrayPosition = 0;
    private int accArrayCounter = 49;
    private int linearAccArrayCounter = 49;
    private boolean firstAccIteration = true;
    private boolean firstLinearAccIteration = true;
    private boolean firstAccSensorChange = true;
    private boolean firstLinearAccSensorChange = true;
    float prevX = 0;
    float prevY = 0;
    float prevZ = 0;

    public SensorsRunnable(Context context) {
        this.context = context;
    }

    public void run(){
        int counter = 0;
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);


        /*
        linearAccEventListener = new SensorEventListener() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSensorChanged(SensorEvent event) {
                String output;
                if (counter == 0){
                    output = "Date,Time,linearAccelerationX,linearAccelerationY,linearAccelerationZ\n";
                    output += LocalDate.now() + "," + LocalTime.now() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "\n";
                } else {
                    output = LocalDate.now() + "," + LocalTime.now() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "\n";
                }

                csvHandler.writeToCSV(context,"LINEAR_ACCELERATION.csv", output);

            }


            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                Log.d("MY_APP", sensor.toString() + " - " + accuracy);
            }


        };
        */

        linearAccEventListener = new SensorEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSensorChanged(SensorEvent event) {
                String output;

                float[] RotationLinearAcc = new float[16];
                float[] InvRotationLinearAcc = new float[16];
                float[] InclinationLinearAcc = new float[16];


                float[] tempLinearAcc= new float[4];
                tempLinearAcc[0] = event.values[0];
                tempLinearAcc[1] = event.values[1];
                tempLinearAcc[2] = event.values[2];
                tempLinearAcc[3] = 0;

                float[] outputWorldLinearAcc = new float[16];

                //https://stackoverflow.com/questions/11578636/acceleration-from-devices-coordinate-system-into-absolute-coordinate-system
                // Get the rotation matrix
                sensorManager.getRotationMatrix(RotationLinearAcc, InclinationLinearAcc, currentGravity, currentMagneticField);
                // Now we have the rotation matrix, rotate the acceleration into the world coordinates
                // First transpose the rotation matrix (also the inverse), since OpenGL uses column major order.
                android.opengl.Matrix.invertM(InvRotationLinearAcc, 0, RotationLinearAcc, 0);


                Matrix.multiplyMV(outputWorldLinearAcc, 0, InvRotationLinearAcc, 0, tempLinearAcc, 0);

                if (firstLinearAccIteration) {
                    if(firstLinearAccSensorChange) {
                        prevlinearAccData3DList.add(new float[1][50][3]);
                        linearAcc3DArrayPosition = 0;
                    }

                    prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][linearAccArrayCounter][0] = outputWorldLinearAcc[0];
                    prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][linearAccArrayCounter][1] = outputWorldLinearAcc[1];
                    prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][linearAccArrayCounter][2] = outputWorldLinearAcc[2];

                    linearAccArrayCounter--;

                    if(linearAccArrayCounter == -1) {
                        linearAccArrayCounter = 49;
                        firstLinearAccIteration = false;
                    }
                    firstLinearAccSensorChange = false;
                } else {
                    prevlinearAccData3DList.add(new float[1][50][3]);
                    linearAcc3DArrayPosition = prevlinearAccData3DList.size() -1;


                    // Moving every element back by 3
                    for(int i =49;i>0;i--) {
                        prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][i][0] = prevlinearAccData3DList.get(linearAcc3DArrayPosition-1)[0][i-1][0];
                        prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][i][1] = prevlinearAccData3DList.get(linearAcc3DArrayPosition-1)[0][i-1][1];
                        prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][i][2] = prevlinearAccData3DList.get(linearAcc3DArrayPosition-1)[0][i-1][2];
                    }


                    if (prevX == outputWorldLinearAcc[0]) {
                        System.out.println("Sensor"+ prevX);
                        System.out.println("Sensor"+ outputWorldLinearAcc[0]);
                        System.out.println("HereX-Sensor");
                    }

                    if (prevY == outputWorldLinearAcc[1]) {
                        System.out.println("Sensor"+ prevY);
                        System.out.println("Sensor"+ outputWorldLinearAcc[1]);
                        System.out.println("HereY--Sensor");

                    }
                    if (prevZ == outputWorldLinearAcc[2]) {
                        System.out.println("Sensor"+ prevZ);
                        System.out.println("Sensor"+ outputWorldLinearAcc[2]);
                        System.out.println("HereZ-Sensor");

                    }
                    prevX = outputWorldLinearAcc[0];
                    prevY = outputWorldLinearAcc[1];
                    prevZ = outputWorldLinearAcc[2];




                    prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][0][0] = outputWorldLinearAcc[0];
                    prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][0][1] = outputWorldLinearAcc[1];
                    prevlinearAccData3DList.get(linearAcc3DArrayPosition)[0][0][2] = outputWorldLinearAcc[2];

                }

                //https://thestatemachine.wordpress.com/2016/07/30/device-motion-in-earth-coordinates/


                output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + tempLinearAcc[0] + "," + tempLinearAcc[1] + "," + tempLinearAcc[2] + "," +"," +outputWorldLinearAcc[0] +"," + outputWorldLinearAcc[1] +"," +outputWorldLinearAcc[2]+"," +outputWorldLinearAcc[3] + "\n";
                csvHandler.writeToCSV(context,"Linear Accelerometer/LINEAR ACCELEROMETER" + LocalDate.now() + ".csv", output);




            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        /*

        public void OnSensorChanged(SensorEvent e)
    {
        if (e.Sensor.Type == SensorType.LinearAcceleration)
        {
            /// e.Values only has 3 entries, we need a vector of size 4 for the rotation matrix. 4th value can be 0.
            for(int i = 0; i &amp;amp;lt; 3; i++)
            {
                DeviceAcceleration[i] = e.Values[i];
            }
        } else if(e.Sensor.Type == SensorType.Gravity)
        {
            GravityMatrix = e.Values.ToArray();
        }
        else if(e.Sensor.Type == SensorType.MagneticField)
        {
            MagneticFieldMatrix = e.Values.ToArray();
        }

        // Only pass on update when it's for the accelerometer
// Translate acceleration into world coordinates
if (e.Sensor.Type == SensorType.LinearAcceleration &amp;amp;amp;&amp;amp;amp; GravityMatrix != null &amp;amp;amp;&amp;amp;amp; MagneticFieldMatrix != null &amp;amp;amp;&amp;amp;amp; DeviceAcceleration != null)
{
    float[] Rotation = new float[16];
    float[] InvRotation = new float[16];
    float[] Inclination = new float[16];

    // Get the rotation matrix
    SensorManager.GetRotationMatrix(Rotation, Inclination, GravityMatrix, MagneticFieldMatrix);

    // Now we have the rotation matrix, rotate the acceleration into the world coordinates
    // First transpose the rotation matrix (also the inverse), since OpenGL uses column major order.
    Android.Opengl.Matrix.TransposeM(InvRotation, 0, Rotation, 0);

    // Get world acceleration

    if (AccelerationEvent != null)
    {

        // Create copy
        List net = new List(WorldAcceleration);

        AccelerationEvent(this, net);
    }
}
}
         */



        accEventListener  = new SensorEventListener() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSensorChanged(SensorEvent event) {
                String output;

                float[] Rotation = new float[16];
                float[] InvRotation = new float[16];
                float[] Inclination = new float[16];


                float[] tempAcc= new float[4];
                tempAcc[0] = event.values[0];
                tempAcc[1] = event.values[1];
                tempAcc[2] = event.values[2];
                tempAcc[3] = 0;

                float[] outputWorldAcc = new float[16];

                //https://stackoverflow.com/questions/11578636/acceleration-from-devices-coordinate-system-into-absolute-coordinate-system
                // Get the rotation matrix
                sensorManager.getRotationMatrix(Rotation, Inclination, currentGravity, currentMagneticField);
                // Now we have the rotation matrix, rotate the acceleration into the world coordinates
                // First transpose the rotation matrix (also the inverse), since OpenGL uses column major order.
                android.opengl.Matrix.invertM(InvRotation, 0, Rotation, 0);


                Matrix.multiplyMV(outputWorldAcc, 0, InvRotation, 0, tempAcc, 0);

                if (firstAccIteration) {
                    if(firstAccSensorChange) {
                        prevAccData3DList.add(new float[1][50][3]);
                        acc3DArrayPosition = 0;
                    }

                    prevAccData3DList.get(acc3DArrayPosition)[0][accArrayCounter][0] = outputWorldAcc[0];
                    prevAccData3DList.get(acc3DArrayPosition)[0][accArrayCounter][1] = outputWorldAcc[1];
                    prevAccData3DList.get(acc3DArrayPosition)[0][accArrayCounter][2] = outputWorldAcc[2];

                    accArrayCounter--;

                    if(accArrayCounter == -1) {
                        accArrayCounter = 49;
                        firstAccIteration = false;
                    }
                    firstAccSensorChange = false;
                } else {
                    prevAccData3DList.add(new float[1][50][3]);
                    acc3DArrayPosition = prevAccData3DList.size() -1;


                    // Moving every element back by 3
                    for(int i =49;i>0;i--) {
                        prevAccData3DList.get(acc3DArrayPosition)[0][i][0] = prevAccData3DList.get(acc3DArrayPosition -1)[0][i-1][0];
                        prevAccData3DList.get(acc3DArrayPosition)[0][i][1] = prevAccData3DList.get(acc3DArrayPosition -1)[0][i-1][1];
                        prevAccData3DList.get(acc3DArrayPosition)[0][i][2] = prevAccData3DList.get(acc3DArrayPosition -1)[0][i-1][2];
                    }


                    if (prevX == outputWorldAcc[0]) {
                        System.out.println("Sensor"+ prevX);
                        System.out.println("Sensor"+ outputWorldAcc[0]);
                        System.out.println("HereX-Sensor");
                    }

                    if (prevY == outputWorldAcc[1]) {
                        System.out.println("Sensor"+ prevY);
                        System.out.println("Sensor"+ outputWorldAcc[1]);
                        System.out.println("HereY--Sensor");

                    }
                    if (prevZ == outputWorldAcc[2]) {
                        System.out.println("Sensor"+ prevZ);
                        System.out.println("Sensor"+ outputWorldAcc[2]);
                        System.out.println("HereZ-Sensor");

                    }
                    prevX = outputWorldAcc[0];
                    prevY = outputWorldAcc[1];
                    prevZ = outputWorldAcc[2];




                    prevAccData3DList.get(acc3DArrayPosition)[0][0][0] = outputWorldAcc[0];
                    prevAccData3DList.get(acc3DArrayPosition)[0][0][1] = outputWorldAcc[1];
                    prevAccData3DList.get(acc3DArrayPosition)[0][0][2] = outputWorldAcc[2];

                }

                //https://thestatemachine.wordpress.com/2016/07/30/device-motion-in-earth-coordinates/


                output = LocalDate.now() + "," + LocalTime.now() + "," + System.nanoTime() + "," + tempAcc[0] + "," + tempAcc[1] + "," + tempAcc[2] + "," +"," +outputWorldAcc[0] +"," + outputWorldAcc[1] +"," +outputWorldAcc[2]+"," +outputWorldAcc[3] + "\n";
                csvHandler.writeToCSV(context,"Accelerometer/ACCELEROMETER" + LocalDate.now() + ".csv", output);



            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                Log.d("MY_APP", sensor.toString() + " - " + accuracy);
            }
        };

        gyroscopeEventListener = new SensorEventListener() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSensorChanged(SensorEvent event) {
                String output;
                if (counter == 0){
                    output = "Date,Time,gyroscopeX,gyroscopeY,gyroscopeZ\n";
                    output += LocalDate.now() + "," + LocalTime.now() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "\n";
                } else {
                    output = LocalDate.now() + "," + LocalTime.now() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "\n";
                }

                csvHandler.writeToCSV(context,"GYROSCOPE.csv", output);

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                Log.d("MY_APP", sensor.toString() + " - " + accuracy);
            }
        };

        magneticFieldEventListener = new SensorEventListener() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSensorChanged(SensorEvent event) {

                currentMagneticField[0] = event.values[0];
                currentMagneticField[1] = event.values[1];
                currentMagneticField[2] = event.values[2];

                String output;
                if (counter == 0){
                    output = "Date,Time,magneticFieldX,magneticFieldY,magneticFieldZ\n";
                    output += LocalDate.now() + "," + LocalTime.now() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "\n";
                } else {
                    output = LocalDate.now() + "," + LocalTime.now() + "," + event.values[0] + "," + event.values[1] + "," + event.values[2] + "\n";
                }

                csvHandler.writeToCSV(context,"MAGNETIC_FIELD.csv", output);

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                Log.d("MY_APP", sensor.toString() + " - " + accuracy);
            }
        };

        gravityEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                currentGravity = event.values;

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

        };

        linearAccSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(linearAccEventListener, linearAccSensor, 20000);


        accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // 50Hz sampling rate. ie 20000 microseconds
        sensorManager.registerListener(accEventListener, accSensor, 20000);

        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorManager.registerListener(gyroscopeEventListener, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);

        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        sensorManager.registerListener(magneticFieldEventListener, magneticSensor, 20000);

        gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        sensorManager.registerListener(gravityEventListener, gravitySensor, 20000);
    }

    public void unregisterListeners(){
        sensorManager.unregisterListener(linearAccEventListener);
        sensorManager.unregisterListener(accEventListener);
        sensorManager.unregisterListener(gyroscopeEventListener);
        sensorManager.unregisterListener(magneticFieldEventListener);
    }


}
