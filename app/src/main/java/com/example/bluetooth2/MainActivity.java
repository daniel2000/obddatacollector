package com.example.bluetooth2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;



public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    boolean stop = false;
    Switch start;
    Switch drivingStyle;

    LocalDate startDate;
    LocalTime startTime;





    private String[] sensorData = new String[4];
    boolean model3D = false;
    private float[][] prevlinearAccData1D = new float[1][150];
    private float[][][] prevlinearAccData3D = new float[1][50][3];
    private int arrayCounter ;
    Thread predictionsThread;
    Thread sensorsThread;
    boolean predictionsThreadRun = true;
    //SensorsRunnable sensorsRunnable;
    CSV csvHandler = new CSV();



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("OBD Data Collector");
        setContentView(R.layout.activity_main);


        if (model3D) {
            arrayCounter = 47;
        } else {
            arrayCounter = 147;
        }


        start = (Switch) findViewById(R.id.startStop);
        drivingStyle = (Switch) findViewById(R.id.DrivingSwitch);

        startDate = LocalDate.now();
        startTime = LocalTime.now();

        //sensorsRunnable = new SensorsRunnable(getApplicationContext());
        //sensorsThread = new Thread(sensorsRunnable);

        //sensorsThread.start();

        //MainPredictionsRunnable predictionsRunnable = new MainPredictionsRunnable(sensorsRunnable);
        //predictionsThread = new Thread(predictionsRunnable);
        //predictionsThread.start();

    }



    public void connectToBT(View view){
        stop = false;
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            public void run(){

                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                // Checking if the device support bluetooth
                if (bluetoothAdapter == null) {
                    // Device doesn't support Bluetooth
                    Toast.makeText(getApplicationContext(), "Your Device doesn't support bluetooth", Toast.LENGTH_LONG).show();
                }


                assert bluetoothAdapter != null;
                // Making sure bluetooth is enabled
                if (!bluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }

                if (REQUEST_ENABLE_BT == 0) {
                    Toast.makeText(getApplicationContext(), "Please allow bluetooth", Toast.LENGTH_LONG).show();
                }

                Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

                ArrayList<String> devices = new ArrayList<>();
                ArrayAdapter arrayAdapter;

                devices.clear();

                //ListView deviceList = (ListView) findViewById(R.id.list);

                if (pairedDevices.size() > 0) {
                    // There are paired devices. Get the name and address of each paired device.
                    for (BluetoothDevice device : pairedDevices) {
                        // If the device if the OBD
                        if (device.getName().equals("VEEPEAK-2006AM83")) {
                            // Connecting to the new socket
                            ConnectThread connectThread = new ConnectThread(device);
                            connectThread.run();

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Connected to ELM 327 device", Toast.LENGTH_LONG).show();
                                }
                            });


                            obtainOBDData(connectThread);

                            //Closing the socket
                            connectThread.cancel();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Stopping socket", Toast.LENGTH_LONG).show();
                                }
                            });

                        }

                        //String deviceHardwareAddress = device.getAddress(); // MAC address
                    }
                }
            }

        }).start();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void drivingStyle(View view){

        if(drivingStyle.isChecked()) {
            drivingStyle.setText("Aggressive Driving Mode");

            String output = "Normal," + startDate+ "," + startTime + "," + LocalDate.now() +"," + LocalTime.now() + "\n";

            csvHandler.writeToCSV(getApplicationContext(),"Driving Style.csv",output);

            startDate = LocalDate.now();
            startTime = LocalTime.now();


        } else {
            drivingStyle.setText("Normal Driving Mode");

            String output = "Aggressive," + startDate+ "," + startTime + "," + LocalDate.now() +"," + LocalTime.now() + "\n";

            csvHandler.writeToCSV(getApplicationContext(),"Driving Style.csv",output);

            startDate = LocalDate.now();
            startTime = LocalTime.now();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void start(View view){
        if(start.isChecked()){
            connectToBT(view);
            start.setText("On");
        } else {
            stop = true;
            start.setText("Off");
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void obtainOBDData(ConnectThread connectThread) {
        while(!stop){
            String outputData = null;
            // Clearing all the previous data
            Arrays.fill(sensorData, null);

            try {
                outputData = connectThread.useSocket(getApplicationContext());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

            if (outputData != null) {

                csvHandler.writeToCSV(getApplicationContext(),"OBDData.csv", outputData);
            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Failed to obtain OBD Data", Toast.LENGTH_LONG).show();
                    }
                });
            }



            /*
            // Delay
            try
            {
                Thread.sleep(2000);
            }
            catch(InterruptedException ex)
            {
                Thread.currentThread().interrupt();
            }

             */

        }
    }

    public void startTraining(View view) {
        //sensorsRunnable.unregisterListeners();
        predictionsThreadRun = false;

        Intent intent = new Intent(view.getContext(), TrainingProcedure.class);
        startActivity(intent);
    }

    /*
    public class MainPredictionsRunnable implements Runnable {

        private SensorsRunnable sensorSet;
        public MainPredictionsRunnable(SensorsRunnable sensorSet) {
            this.sensorSet = sensorSet;
        }



        public void run() {

            TensorFlowLiteMethods tensorFlowLiteMethods = new TensorFlowLiteMethods();


            TupleStringInt returnObj = new TupleStringInt();

            while (predictionsThreadRun) {
                returnObj.predicatedEvent = -1;
                if (sensorSet.accValueUpdated) {
                    if (model3D) {
                        if (arrayCounter != -1) {
                            prevlinearAccData3D[0][arrayCounter][0] = sensorSet.accX;
                            prevlinearAccData3D[0][arrayCounter][1] = sensorSet.accY;
                            prevlinearAccData3D[0][arrayCounter][2] = sensorSet.accZ;
                            arrayCounter--;
                        } else if (arrayCounter == -1) {
                            // Moving every element back by 3
                            for(int i =49;i>0;i--) {
                                prevlinearAccData3D[0][i][0] = prevlinearAccData3D[0][i-1][0];
                                prevlinearAccData3D[0][i][1] = prevlinearAccData3D[0][i-1][1];
                                prevlinearAccData3D[0][i][2] = prevlinearAccData3D[0][i-1][2];
                            }



                            prevlinearAccData3D[0][0][0] = sensorSet.accX;
                            prevlinearAccData3D[0][0][1] = sensorSet.accY;
                            prevlinearAccData3D[0][0][2] = sensorSet.accZ;

                            //arrayCounter = 150;

                            //returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(),null, prevlinearAccData3D,"CNN-Acc0.9683845639228821",true);


                        // Clearing the array
                        //for( int i = 0; i < prevlinearAccData.length; i++ )
                        //    Arrays.fill( prevlinearAccData[i], 0 );



                        }
                    } else {
                        if (arrayCounter != -3) {
                            prevlinearAccData1D[0][arrayCounter] = sensorSet.accX;
                            prevlinearAccData1D[0][arrayCounter + 1] = sensorSet.accY;
                            prevlinearAccData1D[0][arrayCounter + 2] = sensorSet.accZ;
                            arrayCounter -= 3;
                        } else if (arrayCounter == -3) {
                            // Moving every element back by 3
                            for(int i =149;i>2;i--)
                                prevlinearAccData1D[0][i] = prevlinearAccData1D[0][i-3];


                            prevlinearAccData1D[0][0] = sensorSet.accX;
                            prevlinearAccData1D[0][1] = sensorSet.accY;
                            prevlinearAccData1D[0][2] = sensorSet.accZ;
                            //arrayCounter = 150;

                            //returnObj = tensorFlowLiteMethods.predictEventType(getApplicationContext(), prevlinearAccData1D,null, "gruModel93",false);


                        // Clearing the array
                        //for( int i = 0; i < prevlinearAccData.length; i++ )
                        //    Arrays.fill( prevlinearAccData[i], 0 );



                        }
                    }

                    sensorSet.accValueUpdated = false;
                }

                if (returnObj.predicatedEvent != -1 && returnObj.colour != null) {
                    RadioButton nonaggressive = (RadioButton) findViewById(R.id.nonAggressiveBtn);
                    RadioButton rightTurn = (RadioButton) findViewById(R.id.aggRightTurnBtn);
                    RadioButton leftTurn = (RadioButton) findViewById(R.id.aggLeftTurnBtn);
                    RadioButton accelerating = (RadioButton) findViewById(R.id.aggAccelerationBtn);
                    RadioButton braking = (RadioButton) findViewById(R.id.aggBrakingBtn);
                    RadioButton leftLaneChange = (RadioButton) findViewById(R.id.aggLeftLandChangeBtn);
                    RadioButton rightLaneChange = (RadioButton) findViewById(R.id.aggRightLandChangeBtn);


                    if (returnObj.predicatedEvent == 0) {
                        nonaggressive.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 1) {
                        rightTurn.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 2) {
                        leftTurn.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 3) {
                        accelerating.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 4) {
                        braking.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 5) {
                        leftLaneChange.setBackgroundColor(Color.parseColor(returnObj.colour));
                    } else if (returnObj.predicatedEvent == 6) {
                        rightLaneChange.setBackgroundColor(Color.parseColor(returnObj.colour));
                    }

                    // Delay
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }

                    nonaggressive.setBackgroundColor(Color.rgb(0, 255, 0));
                    rightTurn.setBackgroundColor(Color.rgb(0, 255, 0));
                    leftTurn.setBackgroundColor(Color.rgb(0, 255, 0));
                    accelerating.setBackgroundColor(Color.rgb(0, 255, 0));
                    braking.setBackgroundColor(Color.rgb(0, 255, 0));
                    leftLaneChange.setBackgroundColor(Color.rgb(0, 255, 0));
                    rightLaneChange.setBackgroundColor(Color.rgb(0, 255, 0));
                }


            }

        }
    }
    */

}