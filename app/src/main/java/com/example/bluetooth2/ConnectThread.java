package com.example.bluetooth2;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import br.ufrn.imd.obd.commands.ObdCommandGroup;
import br.ufrn.imd.obd.commands.engine.AbsoluteLoadCommand;
import br.ufrn.imd.obd.commands.engine.LoadCommand;
import br.ufrn.imd.obd.commands.engine.MassAirFlowCommand;
import br.ufrn.imd.obd.commands.engine.OilTempCommand;
import br.ufrn.imd.obd.commands.engine.RPMCommand;
import br.ufrn.imd.obd.commands.engine.RelativeThrottlePositionCommand;
import br.ufrn.imd.obd.commands.engine.RuntimeCommand;
import br.ufrn.imd.obd.commands.engine.SpeedCommand;
import br.ufrn.imd.obd.commands.engine.ThrottlePositionCommand;
import br.ufrn.imd.obd.commands.fuel.AirFuelRatioCommand;
import br.ufrn.imd.obd.commands.fuel.ConsumptionRateCommand;
import br.ufrn.imd.obd.commands.fuel.EthanolLevelCommand;
import br.ufrn.imd.obd.commands.fuel.FindFuelTypeCommand;
import br.ufrn.imd.obd.commands.fuel.FuelLevelCommand;
import br.ufrn.imd.obd.commands.fuel.FuelTrimCommand;
import br.ufrn.imd.obd.commands.fuel.WidebandAirFuelRatioCommand;
import br.ufrn.imd.obd.commands.pressure.BarometricPressureCommand;
import br.ufrn.imd.obd.commands.pressure.FuelPressureCommand;
import br.ufrn.imd.obd.commands.pressure.FuelRailPressureCommand;
import br.ufrn.imd.obd.commands.pressure.IntakeManifoldPressureCommand;
import br.ufrn.imd.obd.commands.pressure.PressureCommand;
import br.ufrn.imd.obd.commands.protocol.AdaptiveTimingCommand;
import br.ufrn.imd.obd.commands.protocol.AvailablePidsCommand01to20;
import br.ufrn.imd.obd.commands.protocol.AvailablePidsCommand21to40;
import br.ufrn.imd.obd.commands.protocol.AvailablePidsCommand41to60;
import br.ufrn.imd.obd.commands.protocol.DescribeProtocolCommand;
import br.ufrn.imd.obd.commands.protocol.DescribeProtocolNumberCommand;
import br.ufrn.imd.obd.commands.protocol.EchoOffCommand;
import br.ufrn.imd.obd.commands.protocol.GenericAvailablePidsCommand;
import br.ufrn.imd.obd.commands.protocol.LineFeedOffCommand;
import br.ufrn.imd.obd.commands.protocol.SelectProtocolCommand;
import br.ufrn.imd.obd.commands.protocol.TimeoutCommand;
import br.ufrn.imd.obd.commands.temperature.AirIntakeTemperatureCommand;
import br.ufrn.imd.obd.commands.temperature.AmbientAirTemperatureCommand;
import br.ufrn.imd.obd.commands.temperature.EngineCoolantTemperatureCommand;
import br.ufrn.imd.obd.commands.temperature.TemperatureCommand;
import br.ufrn.imd.obd.enums.ObdProtocols;

public class ConnectThread extends Thread{
    private static final String TAG = "HERE";
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private SensorManager sensorManager;
    private Sensor sensor;

    public ConnectThread(BluetoothDevice device) {
        // Use a temporary object that is later assigned to mmSocket
        // because mmSocket is final.
        BluetoothSocket tmp = null;
        mmDevice = device;

        try {
            // Get a BluetoothSocket to connect with the given BluetoothDevice.
            // MY_UUID is the app's UUID string, also used in the server code.
            UUID MY_UUID = mmDevice.getUuids()[0].getUuid();
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            Log.e(TAG, "Socket's create() method failed", e);
        }
        mmSocket = tmp;
    }

    public void run() {
        // Cancel discovery because it otherwise slows down the connection.
        //bluetoothAdapter.cancelDiscovery();

        try {
            // Connect to the remote device through the socket. This call blocks
            // until it succeeds or throws an exception.
            mmSocket.connect();
        } catch (IOException connectException) {
            // Unable to connect; close the socket and return.
            try {
                mmSocket.close();
            } catch (IOException closeException) {
                Log.e(TAG, "Could not close the client socket", closeException);
            }
            return;
        }

        // The connection attempt succeeded. Perform work associated with
        // the connection in a separate thread.
        //manageMyConnectedSocket(mmSocket);
        return;
    }

    // Closes the client socket and causes the thread to finish.
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the client socket", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String useSocket(Context context) throws IOException, InterruptedException {
        if(mmSocket.isConnected()){

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

            // DO NOT FUNCTION
            // MassAirFlow, Oil temp,
            // ConsumptionRateCommand, Ethanol level command, find fuel type,fuel level command
            // fuel pressure, fuel rail pressure
            // wide band air fuel


            // Group many obd commands into a single command ()
            ObdCommandGroup obdCommands = new ObdCommandGroup();
            obdCommands.add(new EchoOffCommand());
            obdCommands.add(new LineFeedOffCommand());
            //obdCommands.add(new TimeoutCommand(timeout));
            //obdCommands.add(new SelectProtocolCommand(ObdProtocols.ISO_15765_4_CAN));


            // Engine
            obdCommands.add(new AbsoluteLoadCommand());
            obdCommands.add(new LoadCommand());
            //obdCommands.add(new MassAirFlowCommand());
            //obdCommands.add(new OilTempCommand());
            obdCommands.add(new RPMCommand());
            obdCommands.add(new RelativeThrottlePositionCommand());
            obdCommands.add(new RuntimeCommand());
            obdCommands.add(new SpeedCommand());
            obdCommands.add(new ThrottlePositionCommand());


            // Fuel
            obdCommands.add(new AirFuelRatioCommand());
            //obdCommands.add(new ConsumptionRateCommand());
            //obdCommands.add(new EthanolLevelCommand());
            //obdCommands.add(new FindFuelTypeCommand());
            //obdCommands.add(new FuelLevelCommand());
            obdCommands.add(new FuelTrimCommand());
            //obdCommands.add(new WidebandAirFuelRatioCommand());

            // Pressure
            obdCommands.add(new BarometricPressureCommand());
            //obdCommands.add(new FuelPressureCommand());
            //obdCommands.add(new FuelRailPressureCommand());
            obdCommands.add(new IntakeManifoldPressureCommand());

            // Protocol
            //obdCommands.add(new AvailablePidsCommand01to20());
            //obdCommands.add(new AvailablePidsCommand21to40());
            //obdCommands.add(new AvailablePidsCommand41to60());
            obdCommands.add(new DescribeProtocolCommand());
            obdCommands.add(new DescribeProtocolNumberCommand());


            //Temperature
            obdCommands.add(new AirIntakeTemperatureCommand());
            obdCommands.add(new AmbientAirTemperatureCommand());
            obdCommands.add(new EngineCoolantTemperatureCommand());



            // Run all commands at once
            obdCommands.run(mmSocket.getInputStream(), mmSocket.getOutputStream());
            /*
            String output = "Date,Time," +
                    "EchoOff,LineFeedOff,AbsoluteLoad,Load,RPM,RelativeThrottlePosition,Runtime,Speed,ThrottlePosition," +
                    "AirFuelRatio,FuelTrim,WidebandAirFuelRatio," +
                    "BarometricPressure,IntakeManifoldPressure," +
                    "DescribeProtocolCommand,DescribeProtocolNumberCommand," +
                    "AirIntakeTemperature,AmbientAirTemperature,EngineCoolantTemperature\n";
             */

            String output = java.time.LocalDate.now() + "," + java.time.LocalTime.now() + ",";
            output += obdCommands.getFormattedResult();

            output = removeLastCharacter(output);
            output += "\n";


            //WritingToCSV csvFile = new WritingToCSV(context,"OBDData.csv", output);

            //String unitOutput = obdCommands.getResultUnit();
            //WritingToCSV unitCsvFile = new WritingToCSV(context,"OBDDataUnits.csv",unitOutput);

            return output;


        }

        return null;
    }

    public static String removeLastCharacter(String str) {
        String result = null;
        if ((str != null) && (str.length() > 0)) {
            result = str.substring(0, str.length() - 1);
        }
        return result;
    }
}
